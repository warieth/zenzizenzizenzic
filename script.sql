USE [master]
GO
/****** Object:  Database [Zenzizenzizenzic]    Script Date: 2014.11.12. 8:50:52 ******/
CREATE DATABASE [Zenzizenzizenzic]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Zenzizenzizenzic', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Zenzizenzizenzic.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Zenzizenzizenzic_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Zenzizenzizenzic_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Zenzizenzizenzic] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Zenzizenzizenzic].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Zenzizenzizenzic] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET ARITHABORT OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Zenzizenzizenzic] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Zenzizenzizenzic] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Zenzizenzizenzic] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Zenzizenzizenzic] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET RECOVERY FULL 
GO
ALTER DATABASE [Zenzizenzizenzic] SET  MULTI_USER 
GO
ALTER DATABASE [Zenzizenzizenzic] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Zenzizenzizenzic] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Zenzizenzizenzic] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Zenzizenzizenzic] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Zenzizenzizenzic] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Zenzizenzizenzic', N'ON'
GO
USE [Zenzizenzizenzic]
GO
/****** Object:  Table [dbo].[Car]    Script Date: 2014.11.12. 8:50:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Car](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[owner] [int] NOT NULL,
	[plateNumber] [nvarchar](10) NOT NULL,
	[type] [nvarchar](100) NOT NULL,
	[color] [nvarchar](100) NOT NULL,
	[gpsLatitude] [float] NULL,
	[gpsLongitude] [float] NULL,
	[speed5Min] [float] NULL,
	[speed1Hour] [float] NULL,
	[fuelLevel] [float] NULL,
	[kmClock] [float] NULL,
 CONSTRAINT [PK_Car] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Owner]    Script Date: 2014.11.12. 8:50:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Owner](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nchar](100) NOT NULL,
	[email] [nvarchar](100) NOT NULL,
	[address] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Owner] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Car]  WITH CHECK ADD  CONSTRAINT [FK_Car_Owner] FOREIGN KEY([owner])
REFERENCES [dbo].[Owner] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Car] CHECK CONSTRAINT [FK_Car_Owner]
GO
USE [master]
GO
ALTER DATABASE [Zenzizenzizenzic] SET  READ_WRITE 
GO
