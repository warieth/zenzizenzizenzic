﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.Controllers;
using WebApi.Data;
using WebApi.Db;

namespace WebApi.Tests.Controllers
{
    [TestClass]
    public class CarDynamicControllerTest
    {
        [TestMethod]
        public void GetAllCars0()
        {
            // Arrange
            var context = new FakeDbContext
            {
                Car =
                {
                },
                Owner =
                {
                }
            };
            var controller = new CarDynamicController(context);

            // Act
            var result = controller.GetAllCars();

            // Assert
            Assert.IsInstanceOfType(result, typeof(List<CarDynamicValues>));
            Assert.AreEqual(0, ((List<CarDynamicValues>)result).Count);
        }

        [TestMethod]
        public void GetAllCars1()
        {
            // Arrange
            var o1 = new Owner() { id = 1, name = "A B", email = "ab@hu.hu", address = "" };
            var c1 = new Car() { id = 0, owner = 1, plateNumber = "AAA123", color = "blue", Owner1 = o1, 
                fuelLevel = 10, gpsLatitude = 11, gpsLongitude = 20, kmClock = 1000, speed1Hour = 50, speed5Min = 100 };
            
            var context = new FakeDbContext
            {
                Car =
                {
                    c1
                },
                Owner =
                {
                    o1
                }
            };
            var controller = new CarDynamicController(context);

            // Act
            var result = controller.GetAllCars();
            var list = (List<CarDynamicValues>)result;

            // Assert
            Assert.AreEqual(1, list.Count);
            Assert.AreEqual(10, list[0].FuelLevel);
            Assert.AreEqual(11, list[0].GpsLatitude);
            Assert.AreEqual(20, list[0].GpsLongitude);
            Assert.AreEqual(1000, list[0].KmClock);
            Assert.AreEqual(100, list[0].Speed5Min);
            Assert.AreEqual(50, list[0].Speed1Hour);
        }

        [TestMethod]
        public void GetAllCars2()
        {
            // Arrange
            var o1 = new Owner() { id = 1, name = "A B", email = "ab@hu.hu", address = "" };
            var c1 = new Car()
            {
                id = 0,
                owner = 1,
                plateNumber = "AAA123",
                color = "blue",
                Owner1 = o1,
                fuelLevel = 10,
                gpsLatitude = 11,
                gpsLongitude = 20,
                kmClock = 1000,
                speed1Hour = 50,
                speed5Min = 100
            };

            var c2 = new Car()
            {
                id = 1,
                owner = 1,
                plateNumber = "AAA123",
                color = "blue",
                Owner1 = o1,
                fuelLevel = 11,
                gpsLatitude = 12,
                gpsLongitude = 21,
                kmClock = 1001,
                speed1Hour = 51,
                speed5Min = 101
            };

            var context = new FakeDbContext
            {
                Car =
                {
                    c1,
                    c2
                },
                Owner =
                {
                    o1
                }
            };
            var controller = new CarDynamicController(context);

            // Act
            var result = controller.GetAllCars();
            var list = (List<CarDynamicValues>)result;

            // Assert
            Assert.AreEqual(2, list.Count);
            Assert.AreEqual(10, list[0].FuelLevel);
            Assert.AreEqual(11, list[0].GpsLatitude);
            Assert.AreEqual(20, list[0].GpsLongitude);
            Assert.AreEqual(1000, list[0].KmClock);
            Assert.AreEqual(100, list[0].Speed5Min);
            Assert.AreEqual(50, list[0].Speed1Hour);

            Assert.AreEqual(11, list[1].FuelLevel);
            Assert.AreEqual(12, list[1].GpsLatitude);
            Assert.AreEqual(21, list[1].GpsLongitude);
            Assert.AreEqual(1001, list[1].KmClock);
            Assert.AreEqual(101, list[1].Speed5Min);
            Assert.AreEqual(51, list[1].Speed1Hour);
        }

        [TestMethod]
        public void FindCar0()
        {
            // Arrange
            var context = new FakeDbContext
            {
                Car =
                {
                },
                Owner =
                {
                }
            };
            var controller = new CarDynamicController(context);

            // Act
            var result = controller.FindCar(1);

            // Assert
            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public void FindCar1()
        {
            // Arrange
            var o1 = new Owner() { id = 1, name = "A B", email = "ab@hu.hu", address = "" };
            var c1 = new Car()
            {
                id = 0,
                owner = 1,
                plateNumber = "AAA123",
                color = "blue",
                Owner1 = o1,
                fuelLevel = 10,
                gpsLatitude = 11,
                gpsLongitude = 20,
                kmClock = 1000,
                speed1Hour = 50,
                speed5Min = 100
            };

            var context = new FakeDbContext
            {
                Car =
                {
                    c1

                },
                Owner =
                {
                    o1
                }
            };
            var controller = new CarDynamicController(context);

            // Act
            var result = controller.FindCar(0);

            // Assert
            Assert.AreEqual(c1.fuelLevel, result.FuelLevel);
            Assert.AreEqual(c1.gpsLongitude, result.GpsLongitude);
            Assert.AreEqual(c1.gpsLatitude, result.GpsLatitude);
            Assert.AreEqual(c1.speed5Min, result.Speed5Min);
            Assert.AreEqual(c1.speed1Hour, result.Speed1Hour);
            Assert.AreEqual(c1.kmClock, result.KmClock);
        }

        [TestMethod]
        public void PutCar0()
        {
            // Arrange
            var o1 = new Owner() { id = 1, name = "A B", email = "ab@hu.hu", address = "" };
            var c1 = new Car() { id = 0, owner = 1, plateNumber = "AAA123", color = "blue", Owner1 = o1 };
            var context = new FakeDbContext
            {
                Car =
                {
                    c1
                },
                Owner =
                {
                    o1
                }
            };
            var controller = new CarDynamicController(context);
            var cv1 = new CarDynamicValues()
            {
                FuelLevel = 10,
                GpsLatitude = 11,
                GpsLongitude = 20,
                KmClock = 1000,
                Speed1Hour = 50,
                Speed5Min = 100
            };

            // Act
            controller.Put(0, cv1);

            // Assert
            var result = context.Car.Find(0);
            Assert.AreEqual(cv1.FuelLevel, result.fuelLevel);
            Assert.AreEqual(cv1.GpsLatitude, result.gpsLatitude);
            Assert.AreEqual(cv1.GpsLongitude, result.gpsLongitude);
            Assert.AreEqual(cv1.KmClock, result.kmClock);
            Assert.AreEqual(cv1.Speed1Hour, result.speed1Hour);
            Assert.AreEqual(cv1.Speed5Min, result.speed5Min);
        }
    }
}
