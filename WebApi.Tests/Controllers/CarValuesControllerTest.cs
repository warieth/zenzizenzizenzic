﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApi.Db;
using WebApi.Controllers;
using WebApi.Data;
using System.Collections.Generic;

namespace WebApi.Tests.Controllers
{
    [TestClass]
    public class CarValuesControllerTest
    {
        [TestMethod]
        public void GetAllCars0()
        {
            // Arrange
            var context = new FakeDbContext
            {
                Car =
                {
                },
                Owner =
                {
                }
            };
            var controller = new CarValuesController(context);

            // Act
            var result = controller.GetAllCars();

            // Assert
            Assert.IsInstanceOfType(result, typeof(List<CarValues>));
            Assert.AreEqual(0, ((List<CarValues>)result).Count);
        }

        [TestMethod]
        public void GetAllCars1()
        {
            // Arrange
            var o1 = new Owner() { id = 1, name = "A B", email = "ab@hu.hu", address = "" };
            var c1 = new Car() { id = 0, owner = 1, plateNumber = "AAA123", color = "blue", Owner1 = o1 };

            var context = new FakeDbContext
            {
                Car =
                {
                    c1
                },
                Owner = 
                { 
                    o1
                }
            };
            var controller = new CarValuesController(context);

            // Act
            var result = controller.GetAllCars();
            var list = (List<CarValues>)result;

            // Assert
            Assert.AreEqual(1, list.Count);
            Assert.AreEqual(c1.plateNumber, list[0].PlateNumber);
            Assert.AreEqual(o1.id, list[0].OwnerId);
            Assert.AreEqual(c1.color, list[0].Color);
            Assert.AreEqual(c1.type, list[0].Type);
        }

        [TestMethod]
        public void GetAllCars2()
        {
            // Arrange
            var o1 = new Owner() { id = 1, name = "A B", email = "ab@hu.hu", address = "" };
            var c1 = new Car() { id = 0, owner = 1, plateNumber = "AAA123", color = "blue", Owner1 = o1 };
            var c2 = new Car() { id = 1, owner = 1, plateNumber = "ABC576", color = "red", Owner1 = o1 };

            var context = new FakeDbContext
            {
                Car =
                {
                    c1,
                    c2
                },
                Owner = 
                { 
                    o1
                }
            };
            var controller = new CarValuesController(context);

            // Act
            var result = controller.GetAllCars();
            var list = (List<CarValues>)result;

            // Assert
            Assert.AreEqual(2, list.Count);

            Assert.AreEqual(c1.plateNumber, list[0].PlateNumber);
            Assert.AreEqual(o1.id, list[0].OwnerId);
            Assert.AreEqual(c1.color, list[0].Color);
            Assert.AreEqual(c1.type, list[0].Type);

            Assert.AreEqual(c2.plateNumber, list[1].PlateNumber);
            Assert.AreEqual(o1.id, list[1].OwnerId);
            Assert.AreEqual(c2.color, list[1].Color);
            Assert.AreEqual(c2.type, list[1].Type);
        }

        [TestMethod]
        public void GetAllCars3()
        {
            // Arrange
            var o1 = new Owner() { id = 1, name = "A B", email = "ab@hu.hu", address = "" };
            var o2 = new Owner() { id = 4, name = "Zs Zs", email = "zszs@hu.hu", address = "" };
            var c1 = new Car() { id = 0, owner = 1, plateNumber = "AAA123", color = "blue", Owner1 = o1 };
            var c2 = new Car() { id = 1, owner = 1, plateNumber = "ABC576", color = "red", Owner1 = o1 };
            var c3 = new Car() { id = 2, owner = 4, plateNumber = "GHF321", color = "yellow", Owner1 = o2 };

            var context = new FakeDbContext
            {
                Car =
                {
                    c1,
                    c2,
                    c3
                },
                Owner = 
                { 
                    o1,
                    o2
                }
            };
            var controller = new CarValuesController(context);

            // Act
            var result = controller.GetAllCars();
            var list = (List<CarValues>)result;

            // Assert
            Assert.AreEqual(3, list.Count);

            Assert.AreEqual(c1.plateNumber, list[0].PlateNumber);
            Assert.AreEqual(o1.id, list[0].OwnerId);
            Assert.AreEqual(c1.color, list[0].Color);
            Assert.AreEqual(c1.type, list[0].Type);

            Assert.AreEqual(c2.plateNumber, list[1].PlateNumber);
            Assert.AreEqual(o1.id, list[1].OwnerId);
            Assert.AreEqual(c2.color, list[1].Color);
            Assert.AreEqual(c2.type, list[1].Type);

            Assert.AreEqual(c3.plateNumber, list[2].PlateNumber);
            Assert.AreEqual(o2.id, list[2].OwnerId);
            Assert.AreEqual(c3.color, list[2].Color);
            Assert.AreEqual(c3.type, list[2].Type);
        }

        [TestMethod]
        public void FindCar0()
        {
            // Arrange
            var context = new FakeDbContext
            {
                Car =
                {
                },
                Owner =
                {
                }
            };
            var controller = new CarValuesController(context);

            // Act
            var result = controller.FindCar(1);

            // Assert
            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public void FindCar1()
        {
            // Arrange
            var o1 = new Owner() { id = 1, name = "A B", email = "ab@hu.hu", address = "" };
            var c1 = new Car() { id = 0, owner = 1, plateNumber = "AAA123", color = "blue", Owner1 = o1 };
            var c2 = new Car() { id = 1, owner = 1, plateNumber = "AAA123456", color = "red", Owner1 = o1 };

            var context = new FakeDbContext
            {
                Car =
                {
                    c1,
                    c2
                },
                Owner =
                {
                    o1
                }
            };
            var controller = new CarValuesController(context);

            // Act
            var result = controller.FindCar(0);

            // Assert
            Assert.AreEqual(c1.plateNumber, result.PlateNumber);
            Assert.AreEqual(o1.id, result.OwnerId);
            Assert.AreEqual(c1.color, result.Color);
            Assert.AreEqual(c1.type, result.Type);

            // Act
            result = controller.FindCar(1);

            // Assert
            Assert.AreEqual(c2.plateNumber, result.PlateNumber);
            Assert.AreEqual(o1.id, result.OwnerId);
            Assert.AreEqual(c2.color, result.Color);
            Assert.AreEqual(c2.type, result.Type);
        }

        [TestMethod]
        public void PutCar0()
        {
            // Arrange
            var o1 = new Owner() { id = 1, name = "A B", email = "ab@hu.hu", address = "" };
            var context = new FakeDbContext
            {
                Car =
                {
                },
                Owner =
                {
                    o1
                }
            };
            var controller = new CarValuesController(context);
            var cv1 = new CarValues() { 
                Id = 0,
                PlateNumber = "ABC-123",
                Color = "red",
                Type = "",
                OwnerId = 1
            };

            var cv2 = new CarValues()
            {
                Id = 1,
                PlateNumber = "ABC-456",
                Color = "blue",
                Type = "",
                OwnerId = 1
            };

            // Act
            controller.Put(cv1);
            controller.Put(cv2);

            // Assert
            var result = context.Car.Find(0);
            Assert.AreEqual(cv1.PlateNumber, result.plateNumber);
            Assert.AreEqual(cv1.Color, result.color);
            Assert.AreEqual(cv1.Type, result.type);

            result = context.Car.Find(1);
            Assert.AreEqual(cv2.PlateNumber, result.plateNumber);
            Assert.AreEqual(cv2.Color, result.color);
            Assert.AreEqual(cv2.Type, result.type);
        }
    }
}
