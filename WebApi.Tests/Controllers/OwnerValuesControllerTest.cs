﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApi.Db;
using WebApi.Controllers;
using WebApi.Data;
using System.Collections.Generic;

namespace WebApi.Tests.Controllers
{
    [TestClass]
    public class OwnerValuesControllerTest
    {
        [TestMethod]
        public void GetAllOwners0()
        {
            // Arrange
            var context = new FakeDbContext
            {
                Car =
                {
                },
                Owner =
                {
                }
            };
            var controller = new OwnerValuesController(context);

            // Act
            var result = controller.GetAllOwners();

            // Assert
            Assert.IsInstanceOfType(result, typeof(List<OwnerValues>));
            Assert.AreEqual(0, ((List<OwnerValues>)result).Count);
        }

        [TestMethod]
        public void GetAllOwners1()
        {
            // Arrange
            var o1 = new Owner() { id = 1, name = "A B", email = "ab@hu.hu", address = "" };
            var context = new FakeDbContext
            {
                Car =
                {
                },
                Owner =
                {
                    o1
                }
            };
            var controller = new OwnerValuesController(context);

            // Act
            var result = controller.GetAllOwners();
            var list = (List<OwnerValues>)result;

            // Assert
            Assert.AreEqual(1, list.Count);
            Assert.AreEqual(o1.id, list[0].Id);
            Assert.AreEqual(o1.name, list[0].Name);
            Assert.AreEqual(o1.email, list[0].Email);
            Assert.AreEqual(o1.address, list[0].Address);
        }

        [TestMethod]
        public void GetAllOwners2()
        {
            // Arrange
            var o1 = new Owner() { id = 1, name = "A B", email = "ab@hu.hu", address = "" };
            var o2 = new Owner() { id = 4, name = "Zs Zs", email = "zszs@hu.hu", address = "" };
            var context = new FakeDbContext
            {
                Car =
                {
                },
                Owner =
                {
                    o1,
                    o2
                }
            };
            var controller = new OwnerValuesController(context);

            // Act
            var result = controller.GetAllOwners();
            var list = (List<OwnerValues>)result;

            // Assert
            Assert.AreEqual(2, list.Count);

            Assert.AreEqual(o1.id, list[0].Id);
            Assert.AreEqual(o1.name, list[0].Name);
            Assert.AreEqual(o1.email, list[0].Email);
            Assert.AreEqual(o1.address, list[0].Address);

            Assert.AreEqual(o2.id, list[1].Id);
            Assert.AreEqual(o2.name, list[1].Name);
            Assert.AreEqual(o2.email, list[1].Email);
            Assert.AreEqual(o2.address, list[1].Address);
        }

        [TestMethod]
        public void FindOwner0()
        {
            // Arrange
            var context = new FakeDbContext
            {
                Car =
                {
                },
                Owner =
                {
                }
            };
            var controller = new OwnerValuesController(context);

            // Act
            var result = controller.FindOwner(1);

            // Assert
            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public void FindOwner1()
        {
            // Arrange
            var o1 = new Owner() { id = 1, name = "A B", email = "ab@hu.hu", address = "" };
            var o2 = new Owner() { id = 2, name = "A B C", email = "abc@hu.hu", address = "" };

            var context = new FakeDbContext
            {
                Car =
                {
                },
                Owner =
                {
                    o1,
                    o2
                }
            };
            var controller = new OwnerValuesController(context);

            // Act
            var result = controller.FindOwner(1);

            // Assert
            Assert.AreEqual(o1.id, result.Id);
            Assert.AreEqual(o1.name, result.Name);
            Assert.AreEqual(o1.address, result.Address);
            Assert.AreEqual(o1.email, result.Email);

            // Act
            result = controller.FindOwner(2);

            // Assert
            Assert.AreEqual(o2.id, result.Id);
            Assert.AreEqual(o2.name, result.Name);
            Assert.AreEqual(o2.address, result.Address);
            Assert.AreEqual(o2.email, result.Email);
        }

        [TestMethod]
        public void PutOwner0()
        {
            // Arrange
            var context = new FakeDbContext
            {
                Car =
                {
                },
                Owner =
                {
                }
            };
            var controller = new OwnerValuesController(context);
            var ov1 = new OwnerValues()
            {
                Id = 1,
                Name = "A B",
                Email = "ab@hu.hu",
                Address = ""
            };

            var ov2 = new OwnerValues()
            {
                Id = 2,
                Name = "A B C",
                Email = "abc@hu.hu",
                Address = ""
            };

            // Act
            controller.Put(ov1);
            controller.Put(ov2);

            // Assert
            var result = context.Owner.Find(1);
            Assert.AreEqual(ov1.Name, result.name);
            Assert.AreEqual(ov1.Email, result.email);
            Assert.AreEqual(ov1.Address, result.address);

            result = context.Owner.Find(2);
            Assert.AreEqual(ov2.Name, result.name);
            Assert.AreEqual(ov2.Email, result.email);
            Assert.AreEqual(ov2.Address, result.address);
        }
    }
}
