﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace WebApi.Tests
{
    [TestClass]
    public class Integration
    {

        // Futnia kell a WebApi projektnek
        [TestMethod]
        public void ResponseTime()
        {
            //Arrange
            WebRequest request = WebRequest.Create("https://localhost:44300/apiv1/CarStatic");
            ((HttpWebRequest)request).ContentType = "application/json";

            //Act
            var begin = Environment.TickCount;
            var response = request.GetResponse();
            var stream = response.GetResponseStream();
            var end = Environment.TickCount;

            //Assert
            Assert.IsTrue(end - begin <= 2000);
        }

        // Futnia kell a WebApi projektnek
        [TestMethod]
        public void ResponseFormat()
        {
            //Arrange
            WebRequest request = WebRequest.Create("https://localhost:44300/apiv1/CarStatic");
            ((HttpWebRequest)request).ContentType = "application/json";

            //Act
            var response = request.GetResponse();

            //Assert
            var stream = response.GetResponseStream();
            try
            {
                byte[] b;
                using (BinaryReader br = new BinaryReader(stream))
                {
                    b = br.ReadBytes((int)response.ContentLength);
                }
                string json = Encoding.UTF8.GetString(b);
                dynamic stuff = JsonConvert.DeserializeObject(json);
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }
    }
}
