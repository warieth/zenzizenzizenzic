﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Data;
using WebApi.Db;

namespace WebApi.Controllers
{
    public class CarDynamicController : ApiController
    {
        IZenzizenzizenzicEntitiesDb context;

        public CarDynamicController()
        {
            this.context = new ZenzizenzizenzicEntitiesDb();
        }

        public CarDynamicController(IZenzizenzizenzicEntitiesDb context)
        {
            this.context = context;
        }

        //Refaktorálás előtt
//         [HttpGet]
//         [RequireHttps]
//         public IEnumerable<Data.CarDynamicValues> GetAllCars()
//         {
//             List<CarDynamicValues> list = new List<CarDynamicValues>();
// 
//             List<Car> cars = context.Car.Select(p => p).ToList();
// 
//             foreach (Car car in cars)
//             {
//                 list.Add(
//                         new CarDynamicValues
//                         {
//                             KmClock = car.kmClock,
//                             Speed1Hour = car.speed1Hour,
//                             Speed5Min = car.speed5Min,
//                             GpsLatitude = car.gpsLatitude,
//                             GpsLongitude = car.gpsLongitude,
//                             FuelLevel = car.fuelLevel
//                         }
//                     );
// 
//             }
// 
//             return list;
//         }

        //Refaktorálás után
        [HttpGet]
        [RequireHttps]
        public IEnumerable<Data.CarDynamicValues> GetAllCars()
        {
            List<CarDynamicValues> list = new List<CarDynamicValues>();

            List<Car> cars = context.Car.Select(p => p).ToList();

            foreach (Car car in cars)
            {
                list.Add(new CarDynamicValues(car));
            }

            return list;
        }

        //Refaktorálás előtt
//         [HttpGet]
//         [RequireHttps]
//         public CarDynamicValues FindCar(int id)
//         {
//             List<CarDynamicValues> list = new List<CarDynamicValues>();
//             List<Car> cars = context.Car.Select(p => p).ToList();
// 
//             Car car = cars.Find(p => p.id == id);
// 
//             if (car != null)
//             {
//                 return new CarDynamicValues
//                 {
//                     KmClock = car.kmClock,
//                     Speed1Hour = car.speed1Hour,
//                     Speed5Min = car.speed5Min,
//                     GpsLatitude = car.gpsLatitude,
//                     GpsLongitude = car.gpsLongitude,
//                     FuelLevel = car.fuelLevel
//                 };
//             }
//             else
//                 return null;
//         }
        //Refaktorálás után
        [HttpGet]
        [RequireHttps]
        public CarDynamicValues FindCar(int id)
        {
            List<CarDynamicValues> list = new List<CarDynamicValues>();
            List<Car> cars = context.Car.Select(p => p).ToList();

            Car car = cars.Find(p => p.id == id);

            if (car != null)
            {
                return new CarDynamicValues(car);
            }
            else
                return null;
        }
        //Refaktorálás előtt
//         [HttpPut]
//         [RequireHttps]
//         public void Put([FromBody]int id, [FromBody]Data.CarDynamicValues value)
//         {
//             Car car = context.Car.Where(c => c.id == id).SingleOrDefault();
// 
//             if (car != null)
//             {
//                 car.fuelLevel = value.FuelLevel;
//                 car.gpsLatitude = value.GpsLatitude;
//                 car.gpsLongitude = value.GpsLongitude;
//                 car.kmClock = value.KmClock;
//                 car.speed5Min = value.Speed5Min;
//                 car.speed1Hour = value.Speed1Hour;
//             }
//         }
        //Refaktorálás után
        [HttpPut]
        [RequireHttps]
        public void Put([FromBody]int id, [FromBody]Data.CarDynamicValues value)
        {
            Car car = context.Car.Where(c => c.id == id).SingleOrDefault();

            if (car != null)
            {
                car.SetValues(value);
            }
        }
    }
}
