﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebApi.Db;

namespace WebApi.Controllers
{
    public class CarStaticController : ApiController
    {
        IZenzizenzizenzicEntitiesDb context;

        public CarStaticController()
        {
            this.context = new ZenzizenzizenzicEntitiesDb();
        }

        public CarStaticController(IZenzizenzizenzicEntitiesDb context)
        {
            this.context = context;
        }

        //Refaktorálás előtt
//         [HttpGet]
//         [RequireHttps]
//         public IEnumerable<Data.CarStaticReporting> GetAllCars()
//         {
//             List<Data.CarStaticReporting> list = new List<Data.CarStaticReporting>();
//             List<Car> cars = context.Car.Select(p => p).ToList();
// 
//             foreach (Car car in cars)
//             {
//                 list.Add( 
//                         new Data.CarStaticReporting
//                         {
//                             PlateNumber = car.plateNumber,
//                             OwnerName = car.Owner1.name,
//                             Type = car.type,
//                             Color = car.color
//                         }
//                     );
//             }
// 
//             return list;
//         }
        //Refaktorálás után
        [HttpGet]
        [RequireHttps]
        public IEnumerable<Data.CarStaticReporting> GetAllCars()
        {
            List<Data.CarStaticReporting> list = new List<Data.CarStaticReporting>();
            List<Car> cars = context.Car.Select(p => p).ToList();

            foreach (Car car in cars)
            {
                list.Add(new Data.CarStaticReporting(car) );
            }

            return list;
        }

        //Refaktorálás előtt
//         [HttpGet]
//         [RequireHttps]
//         public Data.CarStaticReporting FindCar(int id)
//         {
//             List<Data.CarStaticReporting> list = new List<Data.CarStaticReporting>();
//             List<Car> cars = context.Car.Select(p => p).ToList();
// 
//             Car car = cars.Find(p => p.id == id);
// 
//             if (car != null)
//             {
//                 return new Data.CarStaticReporting
//                 {
//                     PlateNumber = car.plateNumber,
//                     OwnerName = car.Owner1.name,
//                     Type = car.type,
//                     Color = car.color
//                 };
//             }
//             else
//                 return null;
//         }

        //Refaktorálás után
        [HttpGet]
        [RequireHttps]
        public Data.CarStaticReporting FindCar(int id)
        {
            List<Data.CarStaticReporting> list = new List<Data.CarStaticReporting>();
            List<Car> cars = context.Car.Select(p => p).ToList();

            Car car = cars.Find(p => p.id == id);

            if (car != null)
            {
                return new Data.CarStaticReporting(car);
            }
            else
                return null;
        }
    }
}