﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebApi.Db;

namespace WebApi.Controllers
{
    public class OwnerValuesController : ApiController
    {
        IZenzizenzizenzicEntitiesDb context;

        public OwnerValuesController()
        {
            this.context = new ZenzizenzizenzicEntitiesDb();
        }

        public OwnerValuesController(IZenzizenzizenzicEntitiesDb context)
        {
            this.context = context;
        }
        //Refaktorálás előtt
//         [HttpGet]
//         [RequireHttps]
//         public IEnumerable<Data.OwnerValues> GetAllOwners()
//         {
//             List<Data.OwnerValues> list = new List<Data.OwnerValues>();
//             List<Owner> owners = context.Owner.Select(p => p).ToList();
// 
//             foreach (Owner owner in owners)
//             {
//                 list.Add(
//                         new Data.OwnerValues
//                         {
//                             Id = owner.id,
//                             Name = owner.name,
//                             Email = owner.email,
//                             Address = owner.address
//                         }
//                     );
//             }
// 
//             return list;
//         }
        //Refaktorálás után
        [HttpGet]
        [RequireHttps]
        public IEnumerable<Data.OwnerValues> GetAllOwners()
        {
            List<Data.OwnerValues> list = new List<Data.OwnerValues>();
            List<Owner> owners = context.Owner.Select(p => p).ToList();

            foreach (Owner owner in owners)
            {
                list.Add(new Data.OwnerValues(owner));
            }

            return list;
        }

        //Refaktorálás előtt
//         [HttpGet]
//         [RequireHttps]
//         public Data.OwnerValues FindOwner(int id)
//         {
//             List<Data.OwnerValues> list = new List<Data.OwnerValues>();
//             List<Owner> owners = context.Owner.Select(p => p).ToList();
// 
//             Owner owner = owners.Find(p => p.id == id);
// 
//             if (owner != null)
//             {
//                 return new Data.OwnerValues
//                 {
//                     Id = owner.id,
//                     Name = owner.name,
//                     Email = owner.email,
//                     Address = owner.address
//                 };
//             }
//             else
//                 return null;
//         }

        //Refaktorálás után
        [HttpGet]
        [RequireHttps]
        public Data.OwnerValues FindOwner(int id)
        {
            List<Data.OwnerValues> list = new List<Data.OwnerValues>();
            List<Owner> owners = context.Owner.Select(p => p).ToList();

            Owner owner = owners.Find(p => p.id == id);

            if (owner != null)
            {
                return new Data.OwnerValues(owner);
            }
            else
                return null;
        }

//         // Rekaftorálás előtt
//         [HttpPut]
//         [RequireHttps]
//         public void Put([FromBody]Data.OwnerValues value)
//         {
//             Owner owner = context.Owner.Where(o => o.id == value.Id).SingleOrDefault();
// 
//             if (owner == null)
//             {
//                 context.Owner.Add(
//                     new Owner
//                     {
//                         id = value.Id,
//                         name = value.Name,
//                         email = value.Email,
//                         address = value.Address,
//                         Car = new List<Car>()
//                     }
//                 );
//             }
//             else
//             {
//                 owner.id = value.Id;
//                 owner.name = value.Name;
//                 owner.email = value.Email;
//                 owner.address = value.Address;
//             }
//         }
        // Refaktorálás után
        [HttpPut]
        [RequireHttps]
        public void Put([FromBody]Data.OwnerValues value)
        {
            Owner owner = context.Owner.Where(o => o.id == value.Id).SingleOrDefault();

            if (owner == null)
            {
                context.Owner.Add(new Owner(value) );
            }
            else
            {
                owner.SetValues(value);
            }
        }
    }
}