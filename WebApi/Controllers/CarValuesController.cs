﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebApi.Db;

namespace WebApi.Controllers
{
    public class CarValuesController : ApiController
    {
        IZenzizenzizenzicEntitiesDb context;

        public CarValuesController()
        {
            this.context = new ZenzizenzizenzicEntitiesDb();
        }

        public CarValuesController(IZenzizenzizenzicEntitiesDb context)
        {
            this.context = context;
        }


        //Refakt előtti verzó
//         [HttpGet]
//         [RequireHttps]
//         public IEnumerable<Data.CarValues> GetAllCars()
//         {
//             List<Data.CarValues> list = new List<Data.CarValues>();
//             List<Car> cars = context.Car.Select(p => p).ToList();
// 
//             foreach (Car car in cars)
//             {
//                 list.Add(
//                         new Data.CarValues
//                         {
//                             Id = car.id,
//                             PlateNumber = car.plateNumber,
//                             OwnerId = car.owner,
//                             Type = car.type,
//                             Color = car.color
//                         }
//                     );
//             }
// 
//             return list;
//         }

        //Refakt után
        [HttpGet]
        [RequireHttps]
        public IEnumerable<Data.CarValues> GetAllCars()
        {
            List<Data.CarValues> list = new List<Data.CarValues>();
            List<Car> cars = context.Car.Select(p => p).ToList();

            foreach (Car car in cars)
                list.Add(new Data.CarValues(car));

            return list;
        }

        //Refaktorálás előtt
//         [HttpGet]
//         [RequireHttps]
//         public Data.CarValues FindCar(int id)
//         {
//             List<Data.CarValues> list = new List<Data.CarValues>();
//             List<Car> cars = context.Car.Select(p => p).ToList();
// 
//             Car car = cars.Find(p => p.id == id);
// 
//             if (car != null)
//             {
//                 return new Data.CarValues
//                 {
//                     Id = car.id,
//                     PlateNumber = car.plateNumber,
//                     OwnerId = car.owner,
//                     Type = car.type,
//                     Color = car.color
//                 };
//             }
//             else
//                 return null;
//         }

        //Refaktorálás előtt
        [HttpGet]
        [RequireHttps]
        public Data.CarValues FindCar(int id)
        {
            List<Data.CarValues> list = new List<Data.CarValues>();
            List<Car> cars = context.Car.Select(p => p).ToList();

            Car car = cars.Find(p => p.id == id);

            if (car != null)
                return new Data.CarValues(car);
            else
                return null;
        }

//         Refaktorálás előtt
//         [HttpPut]
//         [RequireHttps]
//         public void Put([FromBody]Data.CarValues value)
//         {
//             Car car = context.Car.Where(c => c.id == value.Id).SingleOrDefault();
//             Owner owner = context.Owner.Select(p => p).Where(p => p.id == value.OwnerId).SingleOrDefault();
// 
//             if (owner != null)
//             {
//                 if (car == null)
//                 {
//                     context.Car.Add(
//                         new Car
//                         {
//                             id = value.Id,
//                             plateNumber = value.PlateNumber,
//                             color = value.Color,
//                             type = value.Type,
//                             owner = value.OwnerId,
//                             Owner1 = owner
//                         }
//                     );
//                 }
//                 else
//                 {
//                     car.id = value.Id;
//                     car.plateNumber = value.PlateNumber;
//                     car.color = value.Color;
//                     car.type = value.Type;
//                     car.owner = value.OwnerId;
//                     car.Owner1 = owner;
//                 }
//             }
//         }

        //Refaktorálás után
        [HttpPut]
        [RequireHttps]
        public void Put([FromBody]Data.CarValues value)
        {
            Car car = context.Car.Where(c => c.id == value.Id).SingleOrDefault();
            Owner owner = context.Owner.Select(p => p).Where(p => p.id == value.OwnerId).SingleOrDefault();

            if (owner != null)
            {
                if (car == null)
                {
                    context.Car.Add(new Car(value, owner));
                }
                else
                {
                    car.SetValues(value, owner);
                }
            }
        }

    }
}