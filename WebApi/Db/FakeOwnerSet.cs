﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Db
{
    public class FakeOwnerSet : FakeDbSet<Owner>
    {
        override public Owner Find(params object[] keyValues)
        {
            return this.SingleOrDefault(o => o.id == (int)keyValues.Single());
        }
    }
}