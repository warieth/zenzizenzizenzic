﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Db
{
    public class FakeCarSet : FakeDbSet<Car>
    {
        override public Car Find(params object[] keyValues) 
        {
            return this.SingleOrDefault(c => c.id == (int)keyValues.Single());
        }
    }
}