﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Data;

namespace WebApi.Db
{
	public partial class Car
	{
		public Car(){}

		public Car(CarValues value, Owner owner)
        {
            SetValues(value, owner);
        }

        public void SetValues(CarValues value, Owner owner)
        {
            id = value.Id;
            plateNumber = value.PlateNumber;
            color = value.Color;
            type = value.Type;
            this.owner = value.OwnerId;
            Owner1 = owner;
        }

        public void SetValues(CarDynamicValues value)
        {
            fuelLevel = value.FuelLevel;
            gpsLatitude = value.GpsLatitude;
            gpsLongitude = value.GpsLongitude;
            kmClock = value.KmClock;
            speed5Min = value.Speed5Min;
            speed1Hour = value.Speed1Hour;
        }
	}
}