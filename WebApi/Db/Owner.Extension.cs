﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Data;

namespace WebApi.Db
{
    public partial class Owner
    {
        public Owner(OwnerValues value)
        {
            SetValues(value);
            Car = new List<Car>();
        }

        public void SetValues(OwnerValues value)
        {
            id = value.Id;
            name = value.Name;
            email = value.Email;
            address = value.Address;
        }
    }
}