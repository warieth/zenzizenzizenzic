﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Db
{
    public interface IZenzizenzizenzicEntitiesDb
    {
        IDbSet<Car> Car { get; set; }
        IDbSet<Owner> Owner { get; set; }
    }
}
