﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Db
{
    public class FakeDbContext : IZenzizenzizenzicEntitiesDb
    {
        public FakeDbContext()
        {
            this.Car = new FakeCarSet();
            this.Owner = new FakeOwnerSet();
        }

        public System.Data.Entity.IDbSet<Car> Car
        {
            get;
            set;
        }

        public System.Data.Entity.IDbSet<Owner> Owner
        {
            get;
            set;
        }
    }
}