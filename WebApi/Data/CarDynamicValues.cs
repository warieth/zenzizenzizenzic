﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using WebApi.Db;


namespace WebApi.Data
{
    [DataContract]
    public class CarDynamicValues
    {
        public CarDynamicValues() { }

        public CarDynamicValues(Car car)
        {
            KmClock = car.kmClock;
            Speed1Hour = car.speed1Hour;
            Speed5Min = car.speed5Min;
            GpsLatitude = car.gpsLatitude;
            GpsLongitude = car.gpsLongitude;
            FuelLevel = car.fuelLevel;
        }

        [DataMember]
        public double? GpsLongitude { get; set; }

        [DataMember]
        public double? GpsLatitude { get; set; }

        [DataMember]
        public double? FuelLevel { get; set; }

        [DataMember]
        public double? Speed5Min { get; set; }

        [DataMember]
        public double? Speed1Hour { get; set; }

        [DataMember]
        public double? KmClock { get; set; }
    }
}