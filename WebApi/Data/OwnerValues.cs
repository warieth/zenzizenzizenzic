﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using WebApi.Db;

namespace WebApi.Data
{
    [DataContract]
    public class OwnerValues
    {
        public OwnerValues() { }

        public OwnerValues(Owner owner)
        {
            Id = owner.id;
            Name = owner.name;
            Email = owner.email;
            Address = owner.address;
        }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Address { get; set; }
    }
}