﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using WebApi.Db;

namespace WebApi.Data
{
    [DataContract]
    public class CarStaticReporting
    {

        public CarStaticReporting() { }

        public CarStaticReporting(Car car)
        {
            PlateNumber = car.plateNumber;
            OwnerName = car.Owner1.name;
            Type = car.type;
            Color = car.color;
        }

        [DataMember]
        public string PlateNumber { get; set; }

        [DataMember]
        public string OwnerName { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string Color { get; set; }
    }
}