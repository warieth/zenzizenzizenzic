﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using WebApi.Db;

namespace WebApi.Data
{
    [DataContract]
    public class CarValues
    {
        public CarValues(){}

        public CarValues(int id, string plateNumber, int ownerId, string type, string color)
        {
            Id = id;
            PlateNumber = plateNumber;
            OwnerId = ownerId;
            Type = type;
            Color = color;
        }

        public CarValues (Car car)
        {
            Id = car.id;
            PlateNumber = car.plateNumber;
            OwnerId = car.owner;
            Type = car.type;
            Color = car.color;
        }

        [DataMember]
        public int  Id { get; set; }

        [DataMember]
        public string PlateNumber { get; set; }

        [DataMember]
        public int OwnerId { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string Color { get; set; }
    }
}